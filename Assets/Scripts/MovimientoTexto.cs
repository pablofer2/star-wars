﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MovimientoTexto : MonoBehaviour {

    public Text texto;

    private Vector3 vector3;
	// Use this for initialization
	void Start () {
        vector3 = new Vector3(0, 0.25f, 0.25f);
	}
	
	// Update is called once per frame
	void Update () {
        texto.transform.position += vector3;
	}
}
